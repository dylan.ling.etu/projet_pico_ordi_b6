#include <avr/io.h>			// for the input/output register
#include <avr/interrupt.h> 	

#define NB_TICK		1250

#define SAVE_REGISTER()  \
	asm volatile ( \
		"push	r0				\n\t" \
		"in		r0, __SREG__	\n\t" \
		"push	r0				\n\t" \
		"push	r1				\n\t" \
		"push	r2				\n\t" \
		"push	r3				\n\t" \
		"push	r4				\n\t" \
		"push	r5				\n\t" \
		"push	r6				\n\t" \
		"push	r7				\n\t" \
		"push	r8				\n\t" \
		"push	r9				\n\t" \
		"push	r10				\n\t" \
		"push	r11				\n\t" \
		"push	r12				\n\t" \
		"push	r13				\n\t" \
		"push	r14				\n\t" \
		"push	r15				\n\t" \
		"push	r16				\n\t" \
		"push	r17				\n\t" \
		"push	r18				\n\t" \
		"push	r19				\n\t" \
		"push	r20				\n\t" \
		"push	r21				\n\t" \
		"push	r22				\n\t" \
		"push	r23				\n\t" \
		"push	r24				\n\t" \
		"push	r25				\n\t" \
		"push	r26				\n\t" \
		"push	r27				\n\t" \
		"push	r28				\n\t" \
		"push	r29				\n\t" \
		"push	r30				\n\t" \
		"push	r31				\n\t" \
	);

#define RESTORE_REGISTER() \
	asm volatile (	\
 		"pop r31 				\n\t" \
 		"pop r30 				\n\t" \
 		"pop r29 				\n\t" \
 		"pop r28 				\n\t" \
 		"pop r27 				\n\t" \
 		"pop r26 				\n\t" \
 		"pop r25 				\n\t" \
 		"pop r24 				\n\t" \
 		"pop r23 				\n\t" \
 		"pop r22 				\n\t" \
 		"pop r21 				\n\t" \
 		"pop r20 				\n\t" \
 		"pop r19 				\n\t" \
 		"pop r18 				\n\t" \
 		"pop r17 				\n\t" \
 		"pop r16 				\n\t" \
 		"pop r15 				\n\t" \
 		"pop r14 				\n\t" \
 		"pop r13 				\n\t" \
 		"pop r12 				\n\t" \
 		"pop r11 				\n\t" \
 		"pop r10 				\n\t" \
 		"pop r9 				\n\t" \
 		"pop r8 				\n\t" \
 		"pop r7 				\n\t" \
 		"pop r6 				\n\t" \
 		"pop r5 				\n\t" \
 		"pop r4 				\n\t" \
 		"pop r3 				\n\t" \
 		"pop r2 				\n\t" \
 		"pop r1 				\n\t" \
 		"pop r0 				\n\t" \
 		"out __SREG__, r0 		\n\t" \
 		"pop r0 				\n\t" \
	);

#define MAX_PROCS 2

uint8_t pid=0;
int led_bit_cs3 = 0x20; 
int led_bit_cs2 = 0x01;


//structure ordonnanceur
typedef struct scheduler_t {
  uint16_t pile;
  void (*fonction) (void );
}scheduler_t;

void init_task_led_cs3()
{
	DDRC |= led_bit_cs3; //mettre PC3 en sortie
	PORTC |= 0x00;
}

void task_led_cs3()
{
	PORTC ^= led_bit_cs3;
}

void init_task_led_cs2()
{
	DDRC |= led_bit_cs2; //mettre PC0 en sortie
	PORTC |= 0x00;
}

void task_led_cs2()
{
	PORTC ^= led_bit_cs2;
}


void init_timer()
{
	TCCR1B |= _BV(WGM12); // CTC mode with value in OCR1A 
	TCCR1B |= _BV(CS12);  // CS12 = 1; CS11 = 0; CS10 =1 => CLK/1024 prescaler
	TCCR1B |= _BV(CS10);
	OCR1A  = NB_TICK;
	TIMSK1 |= _BV(OCIE1A);
}


//table des taches
scheduler_t processus[MAX_PROCS]={
  {0x500,task_led_cs2},
  {0x600,task_led_cs3},
  };

/* Fonction qui determine la prochaine tache a executer */
void scheduler()
{
	pid=(pid+1)%MAX_PROCS;
}

ISR(TIMER1_COMPA_vect, ISR_NAKED)
{
    
    SAVE_REGISTER();
    processus[pid].pile=SP;

    scheduler();
    
    SP = processus[pid].pile;
    RESTORE_REGISTER();

    asm volatile("reti");
}

void init_proc(int i)
{
    uint16_t old = SP;

    SP = processus[i].pile;
    int adresse=(int)processus[i].fonction;
    asm volatile("push %0" : : "r" (adresse & 0x00ff) );
    asm volatile("push %0" : : "r" ((adresse & 0xff00)>>8) );
    SAVE_REGISTER();
    processus[i].pile = SP;

    SP = old;
}

int main(void)
{
    init_task_led_cs2(); //initialiser les taches
    init_task_led_cs3();	
    init_timer(); //initialiser le timer
    
    for(int i=0;i<MAX_PROCS;i++) init_proc(i);
    sei();
    processus[0].fonction();
    
    while(1)
    {
	   
    }
    return 0;
}
