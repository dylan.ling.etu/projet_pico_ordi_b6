#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "avr/io.h"
#include "util/delay.h"

#include "serial.h"
#include "spi.h"
#include "SdInfo.h"
#include "Sd2Card.h"


#define CHUNK_SIZE 64
#define CMD_SIZE 100

#define BLOCK_SIZE 512
#define MAX_FILENAME_LENGTH 16
#define MAX_BLOCKS_PER_FILE 16
#define MAX_FILES_IN_DIRECTORY 64
#define MAX_BLOCKS_IN_SUPERBLOCK 1024
#define FILESYSTEM_SIZE (8 * 1024 * 1024) // 8 Mo
#define FIRST_DISPONIBILITY_CARD_BLOCK 1024
#define MAX_BLOCKS_PER_FILE_DESCRIPTION 16
#define FIRST_DATA_BLOCK 1040


void readBlock(SD_info *sd, unsigned int numBlock, int offset, char *storage, int size) {

    readData(sd,numBlock,offset, size, (uint8_t*) storage);
}

void writeBlock(SD_info *sd, unsigned int numBlock, int offset, char *source, int size) {

    char buffer_write[BLOCK_SIZE];
    readData(sd, numBlock, 0, size, (uint8_t *)buffer_write);
    if(offset + size > 512 )
    {
        printf("data to write is too big\n");
        return;
    }
    for (int i = size - 1; i >= 0; i--) {
        source[i + offset] = source[i];
    }
    // Ajouter les zéros au début de la chaîne
    for (int j = 0; j < offset; j++) {
        source[j] = buffer_write[j];
    }
    //free(buffer_write);
    writeBlockSD(sd,numBlock, (uint8_t *)source);

}

void LS(SD_info *sd) {
    char buffer[MAX_FILENAME_LENGTH];
    int fileCount = 0;
    printf("\n");
    // // Parcourir les blocs multiples de 16 à partir de 0 jusqu'à MAX_BLOCKS_IN_SUPERBLOCK
    for (int blockNum = 0; blockNum < MAX_BLOCKS_IN_SUPERBLOCK; blockNum += 16) {
         readBlock(sd,blockNum, 0, buffer, MAX_FILENAME_LENGTH);

         // Vérifier si le nom de fichier est vide
         if (buffer[0] != 0) {
         // Afficher le nom du fichier
            printf("%s\n", buffer);
            fileCount++;
         }
     }
    if (fileCount == 0) {
        printf("\nAucun fichier trouvé.\n");
    }
}


void setBlockAvailability(SD_info *sd, int blockNum, int availability) {

    int byteIndexInCard = blockNum / 8; //Numéro d'octet dans la carte des blocs libres
    int blockIndexInCard = byteIndexInCard / 512; //Numéro du bloc dans la carte des blocs libres
    int byteIndexInBlock = byteIndexInCard % 512; //Numéro d'octet dans le bloc contenant le bit de disponibilité
    int bitOffset = blockNum % 8; //Numéro du bit dans l'octet n°byteIndexInBlock du bloc blockIndexInCard
    
    //indice du bloc contenant le bit à mettre à jour dans le bloc de description
    int availabilityBlockNum = FIRST_DISPONIBILITY_CARD_BLOCK + blockIndexInCard;

    // Lire l'octet existant dans le bloc de disponibilité du super bloc
     char buffer512[BLOCK_SIZE/2];
     char buffer;
    readBlock(sd, availabilityBlockNum, byteIndexInBlock, buffer512, 1);
    buffer = buffer512[0];
    // Mettre à jour le bit d'offset correspondant :
    if (availability==1) { // indisponible
        buffer |= (1 << bitOffset);  // Mettre le bit à 1
        
    } else { // disponible
        buffer &= ~(1 << bitOffset); // Mettre le bit à 0
    }

    // Écrire l'octet mis à jour dans le bloc de disponibilité du super bloc
    writeBlock(sd,availabilityBlockNum, byteIndexInBlock, buffer512, 1);

}

int min(int a, int b) {
    return a < b ? a : b;
}

// Fonction qui reconstitue le numéro de bloc à partir du tableau
int reconstituteNumber(char blockNumberBuffer[2]) {
    char octet1 = blockNumberBuffer[0];
    char octet2 = blockNumberBuffer[1];
    int dataBlockNum = (octet1 << 8) | octet2;
    return dataBlockNum;
}

void RM(SD_info *sd, char *filename) {
    int fileFound = -1;
    int offset;
    char fileBuffer[CHUNK_SIZE];
    
    // Parcourir les blocs réservés pour la description des fichiers (superbloc)
    for (int blockNum = 0; blockNum < MAX_BLOCKS_IN_SUPERBLOCK; blockNum += 16) {
        char filenameBuffer[MAX_FILENAME_LENGTH];
        readBlock(sd,blockNum, 0, filenameBuffer, MAX_FILENAME_LENGTH);
        
        // Vérifier si le bloc contient le nom du fichier recherché
        if (strncmp(filenameBuffer, filename, MAX_FILENAME_LENGTH) == 0) {
            // Effacer le nom du fichier dans le superbloc
            memset(filenameBuffer, 0, MAX_FILENAME_LENGTH);
            writeBlock(sd,blockNum, 0, filenameBuffer, MAX_FILENAME_LENGTH);
            fileFound = 1;
            offset = blockNum;
            break;
        }
    }

    // Fin de fonction si fichier inexistant
    if (fileFound == -1) {
        printf("\nLe fichier \"%s\" n'a pas été trouvé.\n", filename);
        return;
    }

    for (int blockNum = offset+1; blockNum < offset + 16; blockNum++) {

            int chunkSize = 0;
            int chunkStart = 0;
            readBlock(sd,blockNum, 0, fileBuffer, chunkSize);

            int blockNumData = reconstituteNumber(fileBuffer);
                if (blockNumData == 0) {
                    writeBlock(sd,blockNum, chunkStart, fileBuffer, chunkSize);
                    printf("\nLe fichier \"%s\" a été supprimé avec succès.\n", filename);
                    return; // Sortir des boucles
                }
                else setBlockAvailability(sd,blockNumData, 0); // Marquer le bloc comme disponible
                
            writeBlock(sd,blockNum, chunkStart, fileBuffer, chunkSize);
        //}
    }

    printf("\nLe fichier \"%s\" a été supprimé avec succès.\n", filename);
}

// Fonction qui modifie le nom du fichier
void MV(SD_info *sd, char *old_filename, char *new_filename) {
    size_t sizeNew_filename = strlen(new_filename);
    char filenameBuffer[MAX_FILENAME_LENGTH];
    int fileFound = 0;
    
    // Parcourir les blocs réservés pour la description des fichiers (superbloc)
    for (int blockNum = 0; blockNum < MAX_BLOCKS_IN_SUPERBLOCK; blockNum += 1) {
        
        readBlock(sd,blockNum, 0, filenameBuffer, MAX_FILENAME_LENGTH);
        
        // Vérifier si le bloc contient le nom du fichier recherché
        if (strncmp(filenameBuffer, old_filename, MAX_FILENAME_LENGTH) == 0) {
            
            if (sizeNew_filename < MAX_FILENAME_LENGTH) {
                sizeNew_filename+=1;
            }

            // Écrire le nom du fichier dans l'emplacement
            writeBlock(sd,blockNum, 0, new_filename, sizeNew_filename);

            fileFound=1;

            break; // Nom modifié, sortir de la boucle
        }

    }
    if (fileFound == 1) {
        printf("\nLe nom du fichier \"%s\" a été renommé avec succès.\n", old_filename);
    } else {
        printf("\nLe fichier \"%s\" n'a pas été trouvé.\n", old_filename);
    }
}

// Renvoie le premier bloc de donné disponible
int findAvailableBlock(SD_info *sd) {
    char availabilityBuffer[BLOCK_SIZE];
    int offset;

    // Lire la carte de disponibilité (à partir du bloc 1)
    for (int blockNum = FIRST_DISPONIBILITY_CARD_BLOCK; blockNum < FIRST_DATA_BLOCK; blockNum++) {
        readBlock(sd,blockNum, 0, availabilityBuffer, BLOCK_SIZE);
        
        if (blockNum==FIRST_DISPONIBILITY_CARD_BLOCK) {
            offset=FIRST_DATA_BLOCK/8;
        } else {
            offset=0;
        }

        // Parcourir les octets de la carte de disponibilité
        for (int byteIndex = offset; byteIndex < BLOCK_SIZE; byteIndex++) {
            char byte = availabilityBuffer[byteIndex];
            
            // Parcourir les bits de chaque octet
            for (int bitIndex = 0; bitIndex < 8; bitIndex++) {
                // Vérifier si le bit est à 0 (bloc disponible)
                if ((byte & (1 << bitIndex)) == 0) {
                    // Calculer l'indice du bloc en fonction du bloc et du bit
                    int blockIndex = byteIndex * 8 + bitIndex;
                    return blockIndex; 
                }
            }
        }
    }

    // Aucun bloc disponible trouvé
    return -1;
}




// Fonction qui réparti un numéro de bloc sur deux octets
void createNumberBuffer(int dataBlockNum, char blockNumberBuffer[2]) {                    
    blockNumberBuffer[0] = (dataBlockNum >> 8) & 0xFF;
    blockNumberBuffer[1] = dataBlockNum & 0xFF;
}

void TYPE(SD_info *sd, char *filename, char *data) {
    size_t sizeFilename = strlen(filename);
    char buffer[MAX_FILENAME_LENGTH];
    int placeFound = -1;
    int index_description_block = 1; // indice du bloc de descrption
    int block_counter = 1; //compteur de blocs utilisés
    int index_in_descrBlock = 0;
    int offset;
    int lock_block = 0;
    int append_command = -1;
    int blockNum_buffer = 0;
    if (sizeFilename > MAX_FILENAME_LENGTH) {
        printf("\nImpossible de créer le fichier, nom trop long\n");
        return;
    }
    
    // Parcours des blocs réservés pour la description des fichiers (superbloc)
    for (int blockNum = 0; blockNum < MAX_BLOCKS_IN_SUPERBLOCK+16; blockNum += 16) {
        readBlock(sd,blockNum, 0, buffer, MAX_FILENAME_LENGTH);
        // Vérifier si le bloc est vide (pas de nom de fichier)
        if (buffer[0] == 0 && lock_block == 0){
            //printf("\nbloc potentiel trouvé\n");
            blockNum_buffer = blockNum; //au cas où on trouve un bloc libre
            lock_block = 1;//on conserve cette place au cas où il faut créer le fichier
        }
        
        if (strncmp(buffer,filename,strlen(filename))==0) //si même nom de fichier alors on concatenne les data
        {
            append_command = 1;
            char append_description_buffer[CHUNK_SIZE]; // récupère le numéro du bloc dans la description
            char append_buffer[CHUNK_SIZE]; //récupère les données finales à écrire dans le bloc de donnée
            readBlock(sd, blockNum+1, 0, append_description_buffer, 2);
            int dataBlockNum = reconstituteNumber(append_description_buffer);
            readBlock(sd, dataBlockNum, 0, append_buffer, CHUNK_SIZE);
            strcat(append_buffer, data); //former les données finales
            writeBlock(sd, dataBlockNum, 0, append_buffer, CHUNK_SIZE); // écrire dans bloc de donnée, rien à fiare dans description
            break;
        }
        
        if (blockNum == MAX_BLOCKS_IN_SUPERBLOCK) { //si le fichier n'existe pas on le crée
            if (sizeFilename < MAX_FILENAME_LENGTH) {
                sizeFilename += 1; // Ajouter '\0' s'il y a de la place
            }
            writeBlock(sd,blockNum_buffer, 0, filename, sizeFilename);
            placeFound = blockNum_buffer;

            //chercher où mettre les données du
            int dataBlockNum = findAvailableBlock(sd); // Premier bloc de données à partir du bloc 1040

            char dataBlockNumBuffer[CHUNK_SIZE];
            readBlock(sd, dataBlockNum, 0, dataBlockNumBuffer, CHUNK_SIZE);
            while (dataBlockNumBuffer[0] != 0)
            {
                dataBlockNum++;
                readBlock(sd, dataBlockNum, 0, dataBlockNumBuffer, CHUNK_SIZE);
            }
            
            int blockSizeUsed = 0; // Compteur d'octets dans le bloc actuel

            char chunkBuffer[CHUNK_SIZE];
            size_t bytesRead;


            bytesRead = strlen(data);
            strcpy(chunkBuffer,data);
            // Écrire le chunk dans le bloc de données
            writeBlock(sd,dataBlockNum, blockSizeUsed, chunkBuffer, bytesRead);
            setBlockAvailability(sd,dataBlockNum, 1);
            // Écrire le numéro du bloc actuel dans la description du fichier
            char blockNumberBuffer[64];
            createNumberBuffer(dataBlockNum,blockNumberBuffer);
            blockSizeUsed += bytesRead;

            // Si le bloc actuel est plein, passer au bloc suivant OU si c'est le premier tour dans la boucle
            // Ecriture du numéro de bloc utilisé dans les blocs de description
            if (index_description_block == 0) {
                offset = MAX_FILENAME_LENGTH;
            } else {
                offset = 0;
            }
            
            writeBlock(sd,placeFound + index_description_block, offset + index_in_descrBlock*2, blockNumberBuffer, 2);
            index_in_descrBlock++;

            dataBlockNum = findAvailableBlock(sd); // Passer au bloc suivant
            blockSizeUsed = 0; // Réinitialiser la taille utilisée

            // Passage au bloc de description suivant
            if (block_counter == (BLOCK_SIZE/2-offset)) {
                index_description_block++;
                index_in_descrBlock=0;
            }

            // Compteur de nombre de blocs utilisés
            block_counter++;

            // Vérifie si on a atteint le nombre maximal de blocs par fichier
            if (block_counter >= 2040) {
                printf("\nLe fichier a atteint sa taille maximale\n");
                return;
            }

            break; // Fichier créé, sortir de la boucle
        }
            
    }

    if (placeFound != -1) {
        printf("\nLe fichier \"%s\" a été créé avec succès.\n", filename);
    } else {
        if(append_command == 1) printf("\nLes données ont été concaténées dans le fichier %s\n",filename);
        else printf("\nPlus de place dans le système de fichier pour créer ce fichier.\n");
    }


}


void CAT(SD_info *sd, char *filename) {
    char filenameBuffer[MAX_FILENAME_LENGTH];
    char byteBuffer[CHUNK_SIZE];
    char dataBuffer[CHUNK_SIZE];

    
    // Parcours des blocs réservés pour la description des fichiers (superbloc)
    for (int blockNum = 0; blockNum < MAX_BLOCKS_IN_SUPERBLOCK; blockNum += 16) {
        readBlock(sd,blockNum, 0, filenameBuffer, MAX_FILENAME_LENGTH);
        
        // Vérifier si le bloc contient le nom du fichier recherché
        if (strncmp(filenameBuffer, filename, MAX_FILENAME_LENGTH) == 0) {
            // Le nom du fichier a été trouvé
            
            // Parcours les blocs de description
            for (int descrBlockNum=1; descrBlockNum<MAX_BLOCKS_PER_FILE_DESCRIPTION; descrBlockNum++) {
            
               // Lecture des octets deux par deux
                //for (int i=0; i<BLOCK_SIZE;i+=2) {
                    readBlock(sd,descrBlockNum+blockNum, 0, byteBuffer, 2);
                    if (byteBuffer[0] == 0 && byteBuffer[1]== 0) return;
                    
                    // printf("\nByte buffer: %s\n", byteBuffer);
                    //  Lire les numéros de blocs associés à ce fichier depuis les blocs de description
                    int dataBlockNum = reconstituteNumber(byteBuffer);
                   
                    
                    // Lire et afficher le contenu du bloc de données
                    readBlock(sd,dataBlockNum, 0, dataBuffer, CHUNK_SIZE);
                    printf("\n%s\n",dataBuffer);
                    //}
                //}
            }
            
            return; // Fichier affiché, sortie de la fonction
        }
    }
    
    // Si le fichier n'a pas été trouvé
    printf("\nLe fichier \"%s\" n'a pas été trouvé.\n", filename);
}


// Fonction pour copier un fichier existant du système de fichier
void CP(SD_info *sd, char *source_filename, char *destination_filename) {
    char source_filenameBuffer[MAX_FILENAME_LENGTH];
    char destination_filenameBuffer[MAX_FILENAME_LENGTH];
    char descriptionBuffer[CHUNK_SIZE];
    char numBuffer[64];
    int destination_offset;
    int source_offset;
    int numDataBlock;
    int newDataBlock;
    
    // Recherche du fichier source
    source_offset = -1;
    for (int blockNum = 0; blockNum < MAX_BLOCKS_IN_SUPERBLOCK; blockNum += 16) {
        readBlock(sd,blockNum, 0, source_filenameBuffer, MAX_FILENAME_LENGTH);
        
        // Vérifier si le bloc contient le nom du fichier source
        if (strncmp(source_filenameBuffer, source_filename, MAX_FILENAME_LENGTH) == 0) {
            source_offset = blockNum;
            break;
        }
    }
    
    if (source_offset == -1) {
        printf("\nLe fichier source \"%s\" n'a pas été trouvé.\n", source_filename);
        return;
    }

    // Recherche d'un emplacement libre pour le fichier destination
    destination_offset = -1;
    for (int blockNum = 0; blockNum < MAX_BLOCKS_IN_SUPERBLOCK; blockNum += 16) {
        readBlock(sd,blockNum, 0, destination_filenameBuffer, MAX_FILENAME_LENGTH);
        
        // Vérifier si le bloc est vide (pas de nom de fichier)
        if (destination_filenameBuffer[0] == 0) {
            destination_offset = blockNum;
            break;
        }
    }
    
    if (destination_offset == -1) {
        printf("\nPlus de place dans le système de fichier pour créer la copie de \"%s\".\n", source_filename);
        return;
    }

    // Copie du nom de la copie dans le bloc de description
    writeBlock(sd,destination_offset, 0, destination_filename, strlen(destination_filename));
 
        
        // Lecture des numéros de bloc dans les blocs de description
        
            readBlock(sd,source_offset+1, 0, numBuffer, 2);
            

            numDataBlock = reconstituteNumber(numBuffer); 
            //si aucune donnée à copier
            if (numDataBlock == 0) {
                printf("\nLa copie de \"%s\" sous le nom \"%s\" a été créée avec succès.\n", source_filename, destination_filename);
                return;
            }
            
            
                // On stocke le bloc de données associé au fichier source dans descriptionBuffer
                readBlock(sd,numDataBlock, 0, descriptionBuffer, CHUNK_SIZE);
                
                
                    // Trouver un nouveau bloc de données disponible
                    newDataBlock = findAvailableBlock(sd);
                    
                    char dataBlockNumBuffer[CHUNK_SIZE];
                    readBlock(sd, newDataBlock, 0, dataBlockNumBuffer, CHUNK_SIZE);
                    while (dataBlockNumBuffer[0] != 0)
                    {
                        newDataBlock++;
                        readBlock(sd, newDataBlock, 0, dataBlockNumBuffer, CHUNK_SIZE);
                    }

                    // Mise à jour de la carte de disponibilités
                    setBlockAvailability(sd,newDataBlock, 1);
                    
                    // Ecriture du numéro de bloc dans la description du fichier
                    createNumberBuffer(newDataBlock,numBuffer);
                    writeBlock(sd,destination_offset+1, 0, numBuffer, 2);
                
            
                // Ecriture du bloc de données dans le premier bloc disponible
                writeBlock(sd,newDataBlock, 0, descriptionBuffer, CHUNK_SIZE);
                printf("\nLa copie de \"%s\" sous le nom \"%s\" a été créée avec succès.\n", source_filename, destination_filename);
    
}




int main(int argc, char *argv[]) {

    char user_data;
    char current_cmd[CMD_SIZE +1];
    SD_info sd;
    init_printf();
    spi_init();
    sd_init(&sd);
    
    printf("Pico ordinateur OK\n\n");
    
    current_cmd[0] = '\0'; // Initialise la chaîne vide
    printf("PicoOrdi>");
    
    while (1) {

        user_data = get_serial();
       
        if (user_data == '\b'){
            current_cmd[strlen(current_cmd) - 1] = '\0'; // retirer le dernier charactère de la chaine actuelle
            printf("%c", user_data);
        }
        else {
            printf("%c", user_data);
            
            strncat(current_cmd, &user_data, 1); // Ajoute le caractère à la fin de la chaîne
            
            if (user_data == '\r') {

                char delim = ' '; // espace délimite les arguments
                current_cmd[strlen(current_cmd) - 1] = '\0'; // Retire le retour à la ligne
                if (strncmp(current_cmd, "FORMAT", 6) == 0) { 
                    erase(&sd, 0, 15000); //supprimer toutes les données
                    printf("\nToutes les données ont été supprimées\n");
                }
                else if (strncmp(current_cmd, "LS", 2) == 0) { // Compare les deux premiers caractères
                    LS(&sd); //lister les fichiers
                } else if (strncmp(current_cmd, "RM", 2) == 0){
                    char filenameRm[MAX_FILENAME_LENGTH];
                    
                    char * tokenRm = strtok(current_cmd, &delim);
                    tokenRm = strtok(NULL, &delim); //token prend la valeur du nom de fichier
                    strcpy(filenameRm, tokenRm);
                    RM(&sd, filenameRm); // efface le fichier

                } else if (strncmp(current_cmd, "TYPE", 4) == 0){
                    char filenameType[MAX_FILENAME_LENGTH];

                    char dataType[CHUNK_SIZE];
                    char * tokenType = strtok(current_cmd, &delim);
                    tokenType = strtok(NULL, &delim);
                    strcpy(filenameType, tokenType);
                    delim = '/';
                    tokenType = strtok(NULL, &delim);
                    strcpy(dataType, tokenType);
                    TYPE(&sd, filenameType,dataType); //crée le fichier

                } else if (strncmp(current_cmd, "MV", 2) == 0){
                    char filename_old[MAX_FILENAME_LENGTH];
                    char filename_new[MAX_FILENAME_LENGTH];

                    char * tokenMv = strtok(current_cmd, &delim);
                    tokenMv = strtok(NULL, &delim); //prend ancien nom
                    strcpy(filename_old, tokenMv);
                    tokenMv = strtok(NULL, &delim); //prend nouveau nom
                    strcpy(filename_new, tokenMv);
                    MV(&sd,filename_old, filename_new); // renommer le fichier

                } else if (strncmp(current_cmd, "CAT", 3) == 0){
                    char filenameCat[MAX_FILENAME_LENGTH];
                    char * tokenCat = strtok(current_cmd, &delim);
                    tokenCat = strtok(NULL, &delim); //prend nouveau nom
                    strcpy(filenameCat, tokenCat);
                    CAT(&sd,filenameCat); //afficher le fichier

                } else if (strncmp(current_cmd, "CP", 2) == 0){
                    char filename_origin[MAX_FILENAME_LENGTH];
                    char filename_copy[MAX_FILENAME_LENGTH];

                    char * tokenCp = strtok(current_cmd, &delim);
                    tokenCp = strtok(NULL, &delim); //prend ancien nom
                    strcpy(filename_origin, tokenCp);
                    tokenCp = strtok(NULL, &delim); //prend nouveau nom
                    strcpy(filename_copy, tokenCp);
                    CP(&sd,filename_origin, filename_copy); // copier le fichier
                } 
                else
                {
                    printf("\nLa commande entrée %s, n'a pas été reconnue\n", current_cmd);
                }
                printf("\nPicoOrdi>");
                current_cmd[0] = '\0'; // Réinitialise la chaîne
            }
        }
           
    }
}

